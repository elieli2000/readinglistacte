
// User Schema
const userSchema = new mongoose.Schema({
	firstName: String,
	lastName: String,
	username : String,
	password : String,
	email: String
})

const productSchema = new mongoose.Schema({

	name: String,
	description: String,
	price: Number

})
// Model

const User = mongoose.model('User', userSchema)
const productModel = mongoose.model('Product', productSchema)


// Express Middleware
app.use(express.json()) 
app.use(express.urlencoded({extended: true}))

// ROUTES

// Creating a user - route
app.post('/register', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send('A duplicate user found!')
		} else {
			if(req.body.username !== '' && req.body.password !== '') {

				let newUser = new User({
					FirstName:req.body.firstName,
					LastName: req.body.lastName,
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((error, savedUser) => {
					if(error) {
						return res.send(error)
					}
					return res.send('New user registered!')
				})
			} else {
				return res.send('Please provide a Username and Password.')
			}
		}
	})
})

// Getting all users - route
app.get('/users', (req, res) => {
	return User.find({}, (error, result) =>{
		if(error) {
			res.send(error)
		} 
		res.send(result)
	})
})
// Creating a user - route
app.post('/createProduct', (req, res) => {
	User.findOne({username: req.body.username}, (error, result) => {
		if(result != null && result.username == req.body.username) {
			return res.send('A duplicate user found!')
		} else {
			if(req.body.username !== '' && req.body.password !== '') {

				let newUser = new User({
					FirstName: req.body.firstName,
					LastName: req.body.lastName,
					username: req.body.username,
					password: req.body.password
				})

				newUser.save((error, savedUser) => {
					if(error) {
						return res.send(error)
					}
					return res.send('New user registered!')
				})
			} else {
				return res.send('Please provide a Username and Password.')
			}
		}
	})
})

// Getting all product - route
app.get('/products', (req, res) => {
	return createProduct.find({}, (error, result) =>{
		if(error) {
			res.send(error)
		} 
		res.send(result)
	})
})



// Port
app.listen(port, () => console.log(`Server is running at port: ${port}`))



app.get('/users', (req, res) => {
	User.find({}, (error, result) => { 
		if(error){
			return res.send(error)
		} else {
			return res.status(200).json({
				signup: result
			})
		}
	})
})
